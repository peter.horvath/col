var intensityElem;
var graynessElem;
var countElem;
var phaseElem;
var clickmeElem;
var colorsElem;
var rMinElem;

var intensity, grayness, count, phase;

function vec(x, y, z) {
  var ret = new Array();
  ret[0] = x;
  ret[1] = y;
  ret[2] = z;
  return ret;
}

function vec_inv(v) {
  return vec(-v[0], -v[1], -v[2]);
}

function vec_add(a, b) {
  var ret = new Array();
  ret[0] = a[0] + b[0];
  ret[1] = a[1] + b[1];
  ret[2] = a[2] + b[2];
  return ret;
}

function vec_minus(a, b) {
  var ret = new Array();
  ret[0] = a[0] - b[0];
  ret[1] = a[1] - b[1];
  ret[2] = a[2] - b[2];
  return ret;
}

function vec_mul_s(s, v) {
  var ret = new Array();
  ret[0] = s*v[0];
  ret[1] = s*v[1];
  ret[2] = s*v[2];
  return ret;
}

function vec_smul_vec(a,b) {
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

function vec_mul_vec(a, b) {
  var ret = new Array();
  ret[0] = a[1]*b[2] - a[2]*b[1];
  ret[1] = a[2]*b[0] - a[0]*b[2];
  ret[2] = a[0]*b[1] - a[1]*b[0];
  return ret;
}

function vec_abs(a) {
  return Math.sqrt(vec_smul_vec(a,a));
}

function vec_norm(a) {
  return vec_mul_s(1.0/vec_abs(a), a);
}

function vec_dist(a, b) {
  return vec_abs(vec_minus(a,b));
}

function plane_intersect_point(v1, n1, v2, n2) {
  var l = vec_mul_vec(n1, n2);

  //console.log('l=' + JSON.stringify(l));

  var nl1 = vec_mul_vec(n1, l);

  //console.log('nl1=' + JSON.stringify(nl1));

  var nl2 = vec_mul_vec(n2, l);

  //console.log('nl2=' + JSON.stringify(nl2));

  /*
  v1+c1*nl1 - (v2+c2*nl2) = c3*l
  v1-v2 = c1*(-nl1) + c2*nl2 + c3*l
  */

  var v1v2 = math.matrix(vec_minus(v1, v2));
  //console.log(JSON.stringify(v1v2));

  var m = math.matrix([vec_inv(nl1), nl2, l]);
  //console.log(JSON.stringify(m));

  //m  = math.transpose(m);
  //console.log(JSON.stringify(m));
  
  m  = math.inv(m);
  //console.log(JSON.stringify(m));
  
  var res = math.multiply(v1v2, m);
  //console.log(JSON.stringify(res));
  //console.log(JSON.stringify(res._data[0]));

  var c1 = res._data[0];

  /*
  var m = math.trans[vec_inv(nl1), nl2, l];
  var im = math.inv(m);

  */
  return vec_add(v1, vec_mul_s(c1, nl1));
}

function vec_plane_affin_dist(v_src, n_src, v_pl, n_pl) {
  //console.log('vec_plane_affin_dist: ' + JSON.stringify({v_src: v_src, n_src: n_src, v_pl: v_pl, n_pl: n_pl}));
  var is_point = plane_intersect_point(v_src, n_src, v_pl, n_pl);
  //console.log('is_point: '  + JSON.stringify(is_point));
  var ret = vec_dist(v_src, is_point);
  //console.log('ret: ' + ret);
  return ret;
}

var cube_planes = [
  [[0, 0, 0], [1, 0, 0]],
  [[0, 0, 0], [0, 1, 0]],
  [[0, 0, 0], [0, 0, 1]],
  [[1, 1, 1], [1, 0, 0]],
  [[1, 1, 1], [0, 1, 0]],
  [[1, 1, 1], [0, 0, 1]]
];

function setcolor(n, vec) {
  //console.log("vec: " + JSON.stringify(vec));
  var rgb = new Array();
  for (var i = 0; i < 3; i++) {
    if (vec[i] < 0 || vec[i] > 1) {
      console.error('buggy color vec: ' + JSON.stringify(vec));
    }
    rgb[i] = Math.floor(vec[i] * 256);
    if (rgb[i] == 256) {
      rgb[i] = 255;
    }
    rgb[i] = rgb[i].toString(16);
    if (rgb[i].length < 2) {
      rgb[i] = '0' + rgb[i];
    }
  }
  var colorCode = '#' + rgb.join('');
  colorsElem.childNodes[n].style.backgroundColor = colorCode;
}

function calculate_colors() {
  var grayPoint = [intensity, intensity, intensity];
  var eyeNormal = [0.299, 0.587, 0.114];
  var r_min = NaN;
  for (var i=0; i < cube_planes.length; i++) {
    var r = vec_plane_affin_dist(grayPoint, eyeNormal, cube_planes[i][0], cube_planes[i][1]);
    console.log('i=' + i + ' r=' + r + ' r_min=' + r_min);
    if (isNaN(r_min) || r < r_min) {
      r_min = r;
    }
  }
  console.log("r_min=" + r_min);
  rMinElem.innerHTML = r_min;
  var x_vec = vec_norm(vec_mul_vec(eyeNormal, [0, 0, 1]));
  var y_vec = vec_norm(vec_mul_vec(eyeNormal, [0, 1, 0]));
  for (var i=0; i < count; i++) {
    var angle = 1.0 * phase * Math.PI / 180 + 2 * Math.PI * i / count;
    var vec = vec_add(vec_mul_s(Math.cos(angle), x_vec), vec_mul_s(Math.sin(angle), y_vec));
    vec = vec_add(grayPoint, vec_mul_s(r_min * (1 - grayness), vec));
    setcolor(i, vec);
  }
}

/*
function show_r_mins() {
  for (var n = 0; n <= 1; n+= 0.01) {
    var grayPoint = [n, n, n];
    var eyeNormal = [0.299, 0.587, 0.114];
    var r_min = NaN;
    for (var i=0; i<cube_planes.length; i++) {
      var r = vec_plane_affin_dist(grayPoint, eyeNormal, cube_planes[i][0], cube_planes[i][1]);
      if (isNaN(r_min) || r < r_min) {
        r_min = r;
      }
    }
    console.log("n=" + n + " r_min=" + r_min);
  }
}*/

function init() {
  intensityElem = document.getElementById('intensity');
  graynessElem = document.getElementById('grayness');
  countElem = document.getElementById('count');
  phaseElem = document.getElementById('phase');
  clickmeElem = document.getElementById('clickme');
  colorsElem = document.getElementById('colors');
  rMinElem = document.getElementById('r_min');
}

function load_backend_state() {
  intensity = localStorage.getItem('intensity');
  grayness = localStorage.getItem('grayness');
  count = localStorage.getItem('count');
  phase = localStorage.getItem('phase');
}

function save_backend_state() {
  localStorage.setItem('intensity', intensity);
  localStorage.setItem('grayness', grayness);
  localStorage.setItem('count', count);
  localStorage.setItem('phase', phase);
}

function load_frontend_state() {
  intensity = intensityElem.value;
  grayness = graynessElem.value;
  count = countElem.value;
  phase = phaseElem.value;
}

function save_frontend_state() {
  intensityElem.value = intensity;
  graynessElem.value = grayness;
  countElem.value = count;
  phaseElem.value = phase;

  var colsNow = colorsElem.childNodes.length;
  while (colsNow < count) {
    var newColNode = document.createElement("div");
    newColNode.className = "colorNode";
    colorsElem.appendChild(newColNode);
    colsNow++;
  }
  while (colsNow > count) {
    colorsElem.removeChild(colorsElem.lastChild);
    colsNow--;
  }

  calculate_colors();
}

function fix_state() {
  if (typeof intensity == 'string') {
    intensity = parseFloat(intensity);
  }
  if (typeof intensity != 'number' || isNaN(intensity)) {
    intensity = 0.5;
  }
  if (intensity < 0) {
    intensity = 0;
  }
  if (intensity > 1) {
    intensity = 1;
  }

  if (typeof grayness == 'string') {
    grayness = parseFloat(grayness);
  }
  if (typeof grayness != 'number' || isNaN(grayness)) {
    grayness = 0;
  }
  if (grayness < 0) {
    grayness = 0;
  }
  if (grayness > 1) {
    grayness = 1;
  }

  if (typeof count == 'string') {
    count = parseInt(count);
  }
  if (typeof count != 'number' || isNaN(count)) {
    count = 3;
  }
  count = Math.round(count);
  if (count < 2)
    count = 2;
  if (count > 255)
    count = 255;

  if (typeof phase == 'string') {
    phase = parseFloat(phase);
  }
  if (typeof phase != 'number' || isNaN(phase)) {
    phase = 0;
  }
  if (phase < 0) {
    phase = 0;
  }
  if (phase >= 359.9) {
    phase = 359.9;
  }
}

function on_change() {
  load_frontend_state();
  fix_state();
  save_backend_state();
  save_frontend_state();
}

function init_frontend_dynamics() {
  intensityElem.addEventListener('change', on_change);
  graynessElem.addEventListener('change', on_change);
  countElem.addEventListener('change', on_change);
  phaseElem.addEventListener('change', on_change);
  clickmeElem.addEventListener('click', on_change);
}

function col() {
  //show_r_mins();
  init();
  load_backend_state();
  fix_state();
  save_backend_state();
  save_frontend_state();
  init_frontend_dynamics();
}

window.addEventListener('load', col);
