module.exports = function(grunt) {
  'use strict';
  
  grunt.initConfig({
    mkdir: {
      all: {
        options: {
          create: ['target']
        }
      }
    },
    htmlmin: {                                     // Task
      dist: {                                      // Target
        options: {                                 // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {                                   // Dictionary of files
          'target/index.html': 'src/col.html'     // 'destination': 'source'
        }
      },
    },
    less: {
      development: {
        options: {
          //paths: ['assets/css']
          compress: true,
          optimization: 9,
        },
        files: {
          'target/col.css': 'src/col.less'
        }
      }
    },
    copy: {
      main: {
        files: [{
          expand: false,
          src: 'src/col.js',
          dest: 'target/col.js'
        }],
      },
    },
    exec: {
      pack: {
        options: {
          command: 'python',
          args: [
            './node_modules/google-closure-library/closure/bin/build/closurebuilder.py',
            '--root=src',
            '--root=node_modules/google-closure-library/',
            '--output_mode=script',
            '--input=src/col.js',
            '--output_file=target/all.js'
          ],
          timeout: 20
        }
      },
      compile: {
        options: {
          command: 'java',
          args: ['-jar', './node_modules/google-closure-compiler-java/compiler.jar',
            '--compilation_level=ADVANCED',
            '--env=BROWSER',
            '--warning_level=VERBOSE',
            '--language_in=ECMASCRIPT5_STRICT',
            '--language_out=ECMASCRIPT5_STRICT',
            '--strict_mode_input=true',
            //'--jscomp_error=*',
            '--js=target/all.js',
            '--js_output_file=target/col.js',
          ],
          timeout: 20
        }
      },
    },
    jsbeautifier: {
      files: "target/col.js",
      options: {
        dest: "target/beautified.js"
      }
    },
    clean: ['target/']
  });
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-htmlmin");
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-direct-exec");
  grunt.loadNpmTasks("grunt-mkdir");
  grunt.loadNpmTasks("grunt-jsbeautifier");

  grunt.registerTask('default', ['clean', 'mkdir', 'htmlmin', 'less', 'copy', 'exec:pack', 'exec:compile']);
};
