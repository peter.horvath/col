goog.provide('col');

goog.require('goog.dom');
goog.require('goog.math.Matrix');
goog.require('goog.math.Vec3');

/**
 * @struct
 * @constructor
 */
col = function() {
  /** @type {Element} */
  this.intensityElem = null;
  /** @type {Element} */
  this.graynessElem = null;
  /** @type {Element} */
  this.countElem = null;
  /** @type {Element} */
  this.phaseElem = null;
  /** @type {Element} */
  this.clickmeElem = null;
  /** @type {Element} */
  this.colorsElem = null;
  /** @type {Element} */
  this.rMinElem = null;
  /** @type {number} */
  this.intensity = 0;
  /** @type {number} */
  this.grayness = 0;
  /** @type {number} */
  this.count = 0;
  /** @type {number} */
  this.phase = 0;
}

/*
var col.intensityElem;
var col.graynessElem;
var col.countElem;
var col.phaseElem;
var col.clickmeElem;
var col.colorsElem;
var col.rMinElem;

var col.intensity, col.grayness, col.count, col.phase;
*/

/*
col.vec = function(x, y, z) {
  var ret = new Array();
  ret[0] = x;
  ret[1] = y;
  ret[2] = z;
  return ret;
}

col.vec_inv = function(v) {
  return col.vec(-v[0], -v[1], -v[2]);
}

col.vec_add = function(a, b) {
  var ret = new Array();
  ret[0] = a[0] + b[0];
  ret[1] = a[1] + b[1];
  ret[2] = a[2] + b[2];
  return ret;
}

col.vec_minus = function(a, b) {
  var ret = new Array();
  ret[0] = a[0] - b[0];
  ret[1] = a[1] - b[1];
  ret[2] = a[2] - b[2];
  return ret;
}

col.vec_mul_s = function(s, v) {
  var ret = new Array();
  ret[0] = s*v[0];
  ret[1] = s*v[1];
  ret[2] = s*v[2];
  return ret;
}

col.vec_smul_vec = function(a,b) {
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

col.vec_mul_vec = function(a, b) {
  var ret = new Array();
  ret[0] = a[1]*b[2] - a[2]*b[1];
  ret[1] = a[2]*b[0] - a[0]*b[2];
  ret[2] = a[0]*b[1] - a[1]*b[0];
  return ret;
}

col.vec_abs = function(a) {
  return Math.sqrt(col.vec_smul_vec(a,a));
}

col.vec_norm = function(a) {
  return col.vec_mul_s(1.0/col.vec_abs(a), a);
}

col.vec_dist = function(a, b) {
  return col.vec_abs(col.vec_minus(a,b));
}
*/

/**
 * @private
 * @param {goog.math.Matrix} M
 * @param {goog.math.Vec3} v
 */
col.matrix_mul_vec = function(M, v) {
  var ret = [0, 0, 0];
  var vec = v.toArray();
  for (var i = 0; i < 3; i++) {
    for (var j = 0; j < 3; j++) {
      ret[i] += M.getValueAt(i, j) * vec[j];
    }
  }
  return new goog.math.Vec3(vec[0], vec[1], vec[2]);
};

/**
 * @private
 * @param {goog.math.Vec3} v1
 * @param {goog.math.Vec3} n1
 * @param {goog.math.Vec3} v2
 * @param {goog.math.Vec3} n2
 */
col.plane_intersect_point = function(v1, n1, v2, n2) {
  //var l = col.vec_mul_vec(n1, n2);
  var l = goog.math.Vec3.cross(n1, n2);

  //console.log('l=' + JSON.stringify(l));

  var nl1 = goog.math.Vec3.cross(n1, l);

  //console.log('nl1=' + JSON.stringify(nl1));

  var nl2 = goog.math.Vec3.cross(n2, l);

  //console.log('nl2=' + JSON.stringify(nl2));

  //v1+c1*nl1 - (v2+c2*nl2) = c3*l
  //v1-v2 = c1*(-nl1) + c2*nl2 + c3*l

  var v1v2 = v1.clone().subtract(v2);
  //console.log(JSON.stringify(v1v2));

  var m = new goog.math.Matrix([nl1.clone().invert().toArray(), nl2.toArray(), l.toArray()]);
  //console.log(JSON.stringify(m));

  //m  = math.transpose(m);
  //console.log(JSON.stringify(m));
  
  m  = m.getInverse();
  //console.log(JSON.stringify(m));
  
  var res = col.matrix_mul_vec(m, v1v2);
  //console.log(JSON.stringify(res));
  //console.log(JSON.stringify(res._data[0]));

  //var c1 = res._data[0];
  var c1 = res.x;

  //var m = math.trans[vec_inv(nl1), nl2, l];
  //var im = math.inv(m);

  //return col.vec_add(v1, vec_mul_s(c1, nl1));

  return v1.clone().add(nl1.scale(c1));
}

/**
 * @private
 * @param {goog.math.Vec3} v_src
 * @param {goog.math.Vec3} n_src
 * @param {goog.math.Vec3} v_pl
 * @param {goog.math.Vec3} n_pl
 */
col.vec_plane_affin_dist = function(v_src, n_src, v_pl, n_pl) {
  //console.log('vec_plane_affin_dist: ' + JSON.stringify({v_src: v_src, n_src: n_src, v_pl: v_pl, n_pl: n_pl}));
  var is_point = col.plane_intersect_point(v_src, n_src, v_pl, n_pl);
  //console.log('is_point: '  + JSON.stringify(is_point));
  //var ret = col.vec_dist(v_src, is_point);
  var ret = v_src.clone().subtract(is_point).magnitude();
  //console.log('ret: ' + ret);
  return ret;
}

/**
 * @const
 * @type{goog.math.Vec3}
 */
col.eyeNormal = new goog.math.Vec3(0.299, 0.587, 0.114);

/** @const */ col.cube_planes = [
  [new goog.math.Vec3(0, 0, 0), new goog.math.Vec3(1, 0, 0)],
  [new goog.math.Vec3(0, 0, 0), new goog.math.Vec3(0, 1, 0)],
  [new goog.math.Vec3(0, 0, 0), new goog.math.Vec3(0, 0, 1)],
  [new goog.math.Vec3(1, 1, 1), new goog.math.Vec3(1, 0, 0)],
  [new goog.math.Vec3(1, 1, 1), new goog.math.Vec3(0, 1, 0)],
  [new goog.math.Vec3(1, 1, 1), new goog.math.Vec3(0, 0, 1)]
];

/**
 * @private
 * @param {number} n
 * @param {goog.math.Vec3} v
 */
col.prototype.setcolor = function(n, v) {
  //console.log("vec: " + JSON.stringify(vec));
  var rgb = new Array();
  var vec = v.toArray();
  for (var i = 0; i < 3; i++) {
    if (vec[i] < 0 || vec[i] > 1) {
      console.error('buggy color vec: ' + JSON.stringify(vec));
    }
    rgb[i] = Math.floor(vec[i] * 256);
    if (rgb[i] == 256) {
      rgb[i] = 255;
    }
    rgb[i] = rgb[i].toString(16);
    if (rgb[i].length < 2) {
      rgb[i] = '0' + rgb[i];
    }
  }
  var colorCode = '#' + rgb.join('');
  this.colorsElem.childNodes[n].style.backgroundColor = colorCode;
}

/**
 * @private
 */
col.prototype.calculate_colors = function() {
  var grayPoint = new goog.math.Vec3(this.intensity, this.intensity, this.intensity);
  var r_min = NaN;
  for (var i=0; i<col.cube_planes.length; i++) {
    var r = col.vec_plane_affin_dist(grayPoint, col.eyeNormal, col.cube_planes[i][0], col.cube_planes[i][1]);
    console.log('i=' + i + ' r=' + r + ' r_min=' + r_min);
    if (isNaN(r_min) || r < r_min) {
      r_min = r;
    }
  }
  console.log("r_min=" + r_min);
  this.rMinElem.innerHTML = r_min;
  //var x_vec = col.vec_norm(vec_mul_vec(col.eyeNormal, [0, 0, 1]));
  var x_vec = goog.math.Vec3.cross(col.eyeNormal, new goog.math.Vec3(0, 0, 1)).normalize();
  //var y_vec = col.vec_norm(vec_mul_vec(col.eyeNormal, [0, 1, 0]));
  var y_vec = goog.math.Vec3.cross(col.eyeNormal, new goog.math.Vec3(0, 1, 0)).normalize();
  for (var /** {number} */ i=0; i<this.count; i++) {
    var angle = 1.0 * this.phase * Math.PI / 180 + 2 * Math.PI * i / this.count;
    //var vec = col.vec_add(vec_mul_s(Math.cos(angle), x_vec), vec_mul_s(Math.sin(angle), y_vec));
    var vec = x_vec.scale(Math.cos(angle)).add(y_vec.scale(Math.sin(angle)));
    //vec = col.vec_add(grayPoint, vec_mul_s(r_min * (1 - grayness), vec));
    vec = grayPoint.clone().add(vec.clone().scale(r_min*(1-this.grayness)));
    this.setcolor(i, vec);
  }
}

/**
 * @private
 */
col.prototype.show_r_mins = function() {
  for (var n = 0; n <= 1; n+= 0.01) {
    var grayPoint = new goog.math.Vec3(n, n, n);
    var r_min = NaN;
    for (var i=0; i<col.cube_planes.length; i++) {
      var r = col.vec_plane_affin_dist(grayPoint, col.eyeNormal, col.cube_planes[i][0], col.cube_planes[i][1]);
      if (isNaN(r_min) || r < r_min) {
        r_min = r;
      }
    }
    console.log("n=" + n + " r_min=" + r_min);
  }
}

/**
 * @private
 */
col.prototype.load_backend_state = function() {
  this.intensity = window["localStorage"].getItem('intensity');
  this.grayness = window["localStorage"].getItem('grayness');
  this.count = window["localStorage"].getItem('count');
  this.phase = window["localStorage"].getItem('phase');
}

/**
 * @private
 */
col.prototype.save_backend_state = function() {
  window["localStorage"].setItem('intensity', this.intensity);
  window["localStorage"].setItem('grayness', this.grayness);
  window["localStorage"].setItem('count', this.count);
  window["localStorage"].setItem('phase', this.phase);
}

/**
 * @private
 */
col.prototype.load_frontend_state = function() {
  this.intensity = this.intensityElem.value;
  this.grayness = this.graynessElem.value;
  this.count = this.countElem.value;
  this.phase = this.phaseElem.value;
}

/**
 * @private
 */
col.prototype.save_frontend_state = function() {
  this.intensityElem.value = this.intensity;
  this.graynessElem.value = this.grayness;
  this.countElem.value = this.count;
  this.phaseElem.value = this.phase;

  var colsNow = this.colorsElem.childNodes.length;
  while (colsNow < this.count) {
    var newColNode = document.createElement("div");
    newColNode.className = "colorNode";
    this.colorsElem.appendChild(newColNode);
    colsNow++;
  }
  while (colsNow > this.count) {
    this.colorsElem.removeChild(this.colorsElem.lastChild);
    colsNow--;
  }

  this.calculate_colors();
}

/**
 * @private
 */
col.prototype.fix_state = function() {
  if (typeof this.intensity == 'string') {
    this.intensity = parseFloat(this.intensity);
  }
  if (typeof this.intensity != 'number' || isNaN(this.intensity)) {
    this.intensity = 0.5;
  }
  if (this.intensity < 0) {
    this.intensity = 0;
  }
  if (this.intensity > 1) {
    this.intensity = 1;
  }

  if (typeof this.grayness == 'string') {
    this.grayness = parseFloat(this.grayness);
  }
  if (typeof this.grayness != 'number' || isNaN(this.grayness)) {
    this.grayness = 0;
  }
  if (this.grayness < 0) {
    this.grayness = 0;
  }
  if (this.grayness > 1) {
    this.grayness = 1;
  }

  if (typeof this.count == 'string') {
    this.count = parseInt(this.count, 10);
  }
  if (typeof this.count != 'number' || isNaN(this.count)) {
    this.count = 3;
  }
  this.count = Math.round(this.count);
  if (this.count < 2)
    this.count = 2;
  if (this.count > 255)
    this.count = 255;

  if (typeof this.phase == 'string') {
    this.phase = parseFloat(this.phase);
  }
  if (typeof this.phase != 'number' || isNaN(this.phase)) {
    this.phase = 0;
  }
  if (this.phase < 0) {
    this.phase = 0;
  }
  if (this.phase >= 359.9) {
    this.phase = 359.9;
  }
}

/**
 * @private
 */
col.prototype.on_change = function() {
  this.load_frontend_state();
  this.fix_state();
  this.save_backend_state();
  this.save_frontend_state();
}

/**
 * @private
 */
col.prototype.init_frontend_dynamics = function() {
  this.intensityElem.addEventListener('change', this.on_change);
  this.graynessElem.addEventListener('change', this.on_change);
  this.countElem.addEventListener('change', this.on_change);
  this.phaseElem.addEventListener('change', this.on_change);
  this.clickmeElem.addEventListener('click', this.on_change);
}

/**
 * @private
 */
col.prototype.init = function() {
  this.intensityElem = document.getElementById('intensity');
  this.graynessElem = document.getElementById('grayness');
  this.countElem = document.getElementById('count');
  this.phaseElem = document.getElementById('phase');
  this.clickmeElem = document.getElementById('clickme');
  this.colorsElem = document.getElementById('colors');
  this.rMinElem = document.getElementById('r_min');

  //show_r_mins();

  this.load_backend_state();
  this.fix_state();
  this.save_backend_state();
  this.save_frontend_state();
  this.init_frontend_dynamics();
}

col.main = function() {
  window.addEventListener('load', new col().init);
}

goog.exportSymbol('col.main', col.main);
